module.exports =  (obj, cb) => {

  if(obj==undefined || typeof(obj) != 'object' || obj.length==0){
    return obj;
  }

  for( let k in obj){
    let newValue = cb(k, obj[k]);
    obj[k] = newValue;
  }

}