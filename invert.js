module.exports =  (obj) => {

  const data = {};

  if(obj==undefined || typeof(obj) != 'object' || obj.length==0){
    return data;
  }

  for( let k in obj){
    if(typeof obj[k] == 'function' || typeof obj[k] == 'object'){
      continue;
    }
    else if(typeof obj[k] == 'number'){
      let newKey= obj[k].toString();
      data[newKey]= k;
    }else{
      let newKey= obj[k];
      data[newKey]= k;
    }
  }
  return data;
}