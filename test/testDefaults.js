const defaults = require('../defaults.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham'};

const defaultObject = { name: 'Bruce Wayne', age: 36, phone : 123456789, 1 : 'num'};

const result = defaults(testObject, defaultObject);

console.log(result);