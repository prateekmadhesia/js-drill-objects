const map = require('../mapObject.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham'};

map(testObject, (key, value) => {
  if(typeof value == 'number'){
    return 10 + value;
  }else if(typeof value == 'string'){
    return value + "Edited";
  }
});
console.log(testObject);