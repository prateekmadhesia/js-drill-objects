module.exports =  (obj) => {
  let data = [];

  if(obj==undefined || typeof(obj) != 'object' || obj.length==0){
    return data;
  }

  for( let k in obj){
    
    data.push([k, obj[k]]);
  }

  return data;

}