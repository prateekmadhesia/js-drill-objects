module.exports =  (obj, defObj) => {

  if(obj==undefined || defObj==undefined || typeof(obj) != 'object' || obj.length==0){
    return obj;
  }

  for( let k in defObj){

    if(obj[k] == undefined){
      obj[k]=defObj[k];
    }
  }
  return obj;
}