module.exports =  (obj) => {
  let data = [];

  if(obj==undefined || typeof(obj) != 'object' || obj.length==0){
    return data;
  }

  for( let k in obj){

    if(typeof(obj[k]) == 'function')
      continue;
    
    data.push(obj[k]);
  }

  return data;

}